# Ansible ii

Ansible role for installing and configuring the ii irc client.

Currently made to work with CentOS 8 stream Linux.

## ii

ii is a minimalistic FIFO and filesystem based IRC client. 

It creates an irc directory tree with server, channel and nick name directories. In every directory a FIFO file (in) and normal file (out) is placed.

https://git.suckless.org/ii/file/README.html

## Usage

This Ansible script adds a few bash scripts which make it possible to run ii as a daemon and use it using PHP to send alerts to IRC:

```{php}
<?php

class ii {

  private $fn;
  
  function __construct($server = "127.0.0.1", $channel) {
    $this->fn = fopen("/home/user/ii/".$server."/".$channel."/in" ,"w");
  }

  public function alert($message) {
    fwrite($this->fn, $message."\n");
  }
  
  function __destruct() {
    fclose($this->fn);
  }

}


$irc = new ii("irc.example.com", "#alerts");

$irc->alert("Head's up message");
```

It is designed to support privilige separation, that is you don't have to run the irc client with the same user as your website, however you will obviously have to correctly define filesystem permissions.

## Project status

This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.

## Dependencies

Requires dw.php-fpm: https://gitlab.com/dustwolf/dw.php-fpm
(doesn't actually require it per say, but it requires the PHP user to exist)
